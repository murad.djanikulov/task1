import java.util.ArrayList;
import java.util.List;

public class CustomList {
    private List<CustomClass> customClass=new ArrayList<>();


    public List<CustomClass> getCustomClass() {
        return customClass;
    }

    public void setCustomClass(List<CustomClass> customClass) {
        this.customClass = customClass;
    }

    @Override
    public String toString() {
        return "CustomList{" +
                "customClass=" + customClass +
                '}';
    }

}
