import java.util.LinkedList;

public class CustomHashMap{

    static class Entry {
        private Object key;
        private Object value;

        public Entry(Object key, Object value){
            this.key = key;
            this.value = value;
        }
    }
    private LinkedList<Entry>[] entries;

    public CustomHashMap() {
        this.entries = new LinkedList[5];
    }

    public void putElement(Object key, Object value){
        int index=hash(key);
        if (entries[index]==null) {
            entries[index]=new LinkedList<>();
        }
        var list=entries[index];
        for (Entry entry : list) {
            if(entry.key==key){
                entry.value=value;
            }
        }
        Entry entry=new Entry(key, value);
        list.addLast(entry);
    }

    public Object get(Object key){
        Entry entry=getEntry(key);
        return entry!=null?entry.value:null;
    }

    private Entry getEntry(Object key){
        int index=hash(key);
        var list=entries[index];
        if (list!=null) {
            for (Entry entry : list) {
                if (entry.key==key) {
                    return entry;
                }
            }
        }
        return null;
    }
    public boolean delete(Object key){
        int index=hash(key);
        Entry entry=getEntry(key);
        if (entry!=null) {
            entries[index].remove(entry);
            return true;
        }
        return false;
    }

    public int size(){
        int size=0;
        for (LinkedList<Entry> list : entries) {
            if (list!=null) {
                size=size+ list.size();
            }
        }
        return size;
    }

    public String toString() {
        String s="";
        for (LinkedList<Entry> list : entries) {
            if (list!=null) {
                for (Entry entry : list) {
                    s=s+entry.value;
                }
            }
        }
        return s;
    }

    private int hash(Object key){
        return Math.abs(key.hashCode())%entries.length;
    }
}
