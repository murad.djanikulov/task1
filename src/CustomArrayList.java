
import java.util.*;

public class CustomArrayList {
    private Object[] items;
    private int count=0;

    private int defaultSize=10;

    public CustomArrayList() {
        this.items = new Object[defaultSize];
    }
    public void insert(Object item){
        if(count==defaultSize){
            Object[] newItems=new Object[(defaultSize*=2)];
            for (int i = 0; i < count; i++) {
                newItems[i]=items[i];
            }
            items=newItems;
        }
        items[count++]=item;
    }

    public void insertAt(Object item,int index){
        if(index < 0 || index > count){
            throw new IndexOutOfBoundsException();
        }
        if(count==defaultSize){
            Object[] newItems=new Object[(defaultSize*=2)];
            for (int i = 0; i < count; i++) {
                newItems[i]=items[i];
            }
            items=newItems;
        }
        for (int i = count-1; i >=index; i--) {
            items[i+1]=items[i];
        }
        items[index]=item;
    }

    public void deleteAt(int index){

        if(index<0||index>=count){
            throw new IllegalArgumentException();
        }
        //shift elements = ( 1 2 3 4 5 ) => ( 1 2 4 5 [5] )
        for (int i = index; i < count; i++) {
            items[i]=items[i+1];
        }
        count--;
    }

    public Object get(int index){
        return items[index];
    }

    public int size(){
        return count;
    }


//    public CustomHashMap[] toArray() {
//        return Arrays.copyOf(items, count, CustomHashMap[].class);
//
//    }
    public String print(){
        String str="";
        for (int i = 0; i < count; i++) {
            str += items[i] + ",";
        }
        return str;
    }
    public <T> void sort(Comparator<? super T> c) {
        Arrays.sort((T[]) items, 0, count, c);
    }
}
