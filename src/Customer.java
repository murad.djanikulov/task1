public class Customer {
    private int numeration;
    private String category;

    public Customer(int numeration, String category) {
        this.numeration = numeration;
        this.category = category;
    }

    public int getNumeration() {
        return numeration;
    }

    public String getCategory() {
        return category;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "numeration=" + numeration +
                ", category='" + category + '\'' +
                '}';
    }
}
