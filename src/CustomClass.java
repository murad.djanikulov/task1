public class CustomClass {
    private Integer numeration;
    private String category;

    public CustomClass(Integer numeration, String category) {
        this.numeration = numeration;
        this.category = category;
    }

    public Integer getNumeration() {
        return numeration;
    }


    public String getCategory() {
        return category;
    }


    @Override
    public String toString() {
        return "CustomClass{" +
                "numeration=" + numeration +
                ", category='" + category + '\'' +
                '}';
    }
}
