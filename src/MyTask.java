import org.junit.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.*;

import static org.junit.Assert.assertEquals;

public class MyTask {
    public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException {

        String url = "jdbc:postgresql://localhost:5432/postgres";
        String dbPassword = "1";
        String dbUsername = "postgres";


        Class.forName("org.postgresql.Driver");
        Connection connection = DriverManager.getConnection(url, dbUsername, dbPassword);
        Statement statement = connection.createStatement();
        String create = "create table if not exists customers ( id serial primary key, numeration integer not null, category varchar not null)";
        String insert = "insert into customers(numeration, category) values('10','B'),('11','C'),('12','B'),('13','A'),('14','C'),('15','A')";

        statement.execute(create);
        System.out.println(statement.executeUpdate(insert));


        String select = "select numeration,category from customers order by category asc,numeration asc";

        ResultSet resultSet = statement.executeQuery(select);

        List<Customer> customerList = new ArrayList<>();

        while (resultSet.next()) {
            customerList.add(new Customer(resultSet.getInt("numeration"), resultSet.getString("category")));
        }

        customerList.forEach(System.out::println);

        statement.close();
        connection.close();

        first();
        second();
        third();
        fourth();

    }
    static List<Map<String, Object>> list = new ArrayList<>();

    public static void first() throws IOException, SQLException {

        Scanner sc = new Scanner(new File("folder/demo.csv"));
        sc.useDelimiter(",");

        List temp = new ArrayList();
        while (sc.hasNext()) {
            temp.add(sc.next());
        }
        sc.close();

        Map<String, Object> map = new HashMap<>();

        for (int i = 0; i < temp.size(); i = i + 2) {
            map = new HashMap<>();
            map.put("Numeration", temp.get(i));
            map.put("Category", temp.get(i + 1));
            list.add(map);
        }

        list.sort(Comparator.comparing((Map map1) -> (String) map1.get("Category"))
                .thenComparing((Map map2) -> (String) map2.get("Numeration"))
        );
        System.out.println("First method");

        list.forEach(System.out::println);

    }

    public static void second() throws IOException {

        String line = "";
        BufferedReader br = new BufferedReader(new FileReader("folder/demo.csv"));
        List<String> tempList = new ArrayList<>();
        while ((line = br.readLine()) != null) {
            tempList = Arrays.asList(line.split(","));
        }

        List data = new ArrayList<>();

        for (int i = 0; i < tempList.size(); i = i + 2) {
            data.add(new Customer(Integer.parseInt(tempList.get(i).trim()), tempList.get(i + 1)));
        }
        data.sort(Comparator.comparing(Customer::getCategory)
                .thenComparing(Customer::getNumeration)
        );

        System.out.println("Second method");

        data.forEach(System.out::println);

    }

    public static void third() {
        CustomList customList = new CustomList();
        List<CustomClass> customClass = customList.getCustomClass();
        customClass.add(new CustomClass(1, "B"));
        customClass.add(new CustomClass(2, "A"));
        customClass.add(new CustomClass(3, "C"));
        customClass.add(new CustomClass(4, "A"));
        customClass.add(new CustomClass(5, "C"));
        customClass.add(new CustomClass(6, "B"));

        customList.setCustomClass(customClass);

        customClass.sort(Comparator.comparing(CustomClass::getCategory).thenComparing(CustomClass::getNumeration));

        System.out.println("Third method");

        customList.getCustomClass().forEach(System.out::println);
    }

    static CustomArrayList customList = new CustomArrayList();

    public static void fourth() throws IOException {

        Scanner sc = new Scanner(new File("folder/demo.csv"));
        sc.useDelimiter(",");

        CustomArrayList temp = new CustomArrayList();

        while (sc.hasNext()) {
            temp.insert(sc.next());
        }
        sc.close();

        CustomHashMap map;

        for (int i = 0; i < temp.size(); i = i + 2) {
            map = new CustomHashMap();
            map.putElement("Numeration", temp.get(i));
            map.putElement("Category", temp.get(i + 1));
            customList.insert(map);
        }

        customList.sort(Comparator.comparing((CustomHashMap customHashMap)->(String)customHashMap.get("Category")));

        System.out.println("Fourth method");

        System.out.println(customList.print());


    }
    @Test
    public void printCategory() throws SQLException, IOException {
        first();
        List temp=new ArrayList<>();
        for (Map<String, Object> map : list) {
            temp.add(map.get("Category"));
        }
        System.out.println("Test case");
        assertEquals(temp.toString(),"[ \"A\",  \"A\",  \"B\",  \"B\",  \"C\",  \"C\"]");
    }
    @Test
    public void printNumeration() throws SQLException, IOException {
        first();
        list.sort(Comparator.comparing((Map map2) -> (String) map2.get("Numeration")));
        List temp=new ArrayList<>();
        for (Map<String, Object> map : list) {
            temp.add(map.get("Numeration"));
        }
        System.out.println("Test case");
        assertEquals(temp.toString(),"[ 1,  2,  3,  4,  5,  6]");
    }

    @Test
    public void printAll() throws SQLException, IOException {
        fourth();
        System.out.println("Test case");
        assertEquals(customList.print()," 3 \"A\", 5 \"A\", 1 \"B\", 6 \"B\", 2 \"C\", 4 \"C\",");
    }


}